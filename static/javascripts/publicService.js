/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var public_service=angular.module('myApp.publicService', []);
    public_service.factory('HttpService', HttpServiceFun);
    HttpServiceFun.$inject=['$http', '$rootScope', 'CacheService'];
    var baseUrl="http://52.74.163.60/api/";
    function HttpServiceFun($http, $rootScope, CacheService)
    {
        
        var HttpApiService  = {
                PublicServiceURL : PublicServiceURL,
                PublicServiceURLwithData : PublicServiceURLwithData,
                PrivateServiceURL : PrivateServiceURL,
                PostPrivateServiceURL : PostPrivateServiceURL
            }

            //***************Private API********
            function PrivateServiceURL(url,  method){
                
                if(!CacheService.isCache("client_secret"))
                {
                    alert("Please Login First");
                    return;
                }
                else
                {
                    var clientdata = (JSON.parse(CacheService.getCache("client_secret")).length>0?JSON.parse(CacheService.getCache("client_secret"))[0]:JSON.parse(CacheService.getCache("client_secret")));

                    return $http({
                        url:$rootScope.static_server_path+url,
                        method:method,
                        headers:{
                            'Content-Type': 'application/json',
                            'Authorization': clientdata.token.token_type +" " + clientdata.token.access_token
                        }
                    });
                }
            }

            function PostPrivateServiceURL(url, data, method){

                if(!CacheService.isCache("client_secret"))
                {
                    alert("Please Login First");
                    return;
                }
                else
                {
                    var clientdata = (JSON.parse(CacheService.getCache("client_secret")).length>0?JSON.parse(CacheService.getCache("client_secret"))[0]:JSON.parse(CacheService.getCache("client_secret")));
                    return $http({
                        url:$rootScope.static_server_path+url,
                        method:method,
                        data : data,
                        headers:{
                            'Content-Type': 'application/json',
                            'Authorization': clientdata.token.token_type +" " + clientdata.token.access_token
                        }
                    });
                }
            }


            //***************Public API********
            
            function PublicServiceURL(url){

            	return $http({

                    url:$rootScope.static_server_path+url,
                    method:'GET',
                    headers:{
                        'Content-Type':'application/json'
                    }

                });
            }

            function PublicServiceURLwithData(url, data_){

                return $http({

                    url:$rootScope.static_server_path+url,
                    method:'POST',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    data : data_

                });
            }
        return HttpApiService;
    }

})();

