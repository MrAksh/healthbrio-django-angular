/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var medication_service=angular.module('myApp.medications.services');
    medication_service.factory('MedicationsService', medicationsServiceFun);
    medication_service.factory('MedicationsOverview', medicationsOverviewFun);
    medicationsServiceFun.$inject=['HttpService'];
    medicationsOverviewFun.$inject=['HttpService'];
    
    function medicationsServiceFun(HttpService)
    {
        var conditionApiSerivce  = {
                all : all,
                filterService : filterService
            }

            function all(offsetValue){
                
                return (HttpService.PublicServiceURL('api/medications/?offset='+offsetValue));

            }

            function filterService(filterText){
                return (HttpService.PublicServiceURL('api/medications/filter/'+filterText+"?limit=5000"));
            }

        return conditionApiSerivce;
    }

    function medicationsOverviewFun(HttpService)
    {
        var conditionOverviewApiSerivce  = {
                all : all
            }

            function all($stateParams){
                
                return (HttpService.PublicServiceURL('api/medications/'+$stateParams.medication_id));

            }

        return conditionOverviewApiSerivce;
    }

})();