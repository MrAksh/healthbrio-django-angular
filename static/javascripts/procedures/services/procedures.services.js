/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var procedure_service=angular.module('myApp.procedures.services');
    procedure_service.factory('ProceduresService', ProceduresServiceFun);
    procedure_service.factory('ProceduresOverview', proceduresOverviewFun);
    
    ProceduresServiceFun.$inject=['HttpService'];
    proceduresOverviewFun.$inject=['HttpService'];
    
    function ProceduresServiceFun(HttpService)
    {
        var procedureApiSerivce  = {
                all : all,
                filterService : filterService
            }

            function all(offsetValue){
                return (HttpService.PublicServiceURL('api/procedures/?offset=' +offsetValue));
            }
            function filterService(filterText){
                
                return (HttpService.PublicServiceURL('api/procedures/filter/'+filterText+"?limit=5000"));
            }

        return procedureApiSerivce;
    }

    function proceduresOverviewFun(HttpService)
    {
        var proceduresOverviewApiSerivce  = {
                all : all
            }

            function all($stateParams){
                
                return (HttpService.PublicServiceURL('api/procedures/'+$stateParams.procedure_id));

            }

        return proceduresOverviewApiSerivce;
    }

})();