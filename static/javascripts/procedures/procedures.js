/**
 * Created by IDCS12 on 3/23/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.procedures',[
        'myApp.procedures.controller',
        'myApp.procedures.directives',
        'myApp.procedures.services'
    ]);


    console.log("procedures getter")
    angular.module('myApp.procedures.controller',[]);

    angular.module('myApp.procedures.directives',[]);

    angular.module('myApp.procedures.services',[]);
})();