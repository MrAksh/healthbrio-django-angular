/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var sym_service=angular.module('myApp.symptoms.services',[]);
    sym_service.factory('symptomsService', symptomsServiceFun);
    sym_service.factory('causesService', causesServiceFun);
    sym_service.factory('conditionsService', conditionsServiceFun);
    sym_service.factory('AvatarService', avatarServiceFun);
    sym_service.service('ImageGet', ImageGet);
    symptomsServiceFun.$inject = ['$http','HttpService'];
    causesServiceFun.$inject = ['CacheService', 'HttpService'];
    conditionsServiceFun.$inject = ['HttpService'];
    ImageGet.$inject = ['$http'];
    avatarServiceFun.$inject=['HttpService'];

    function symptomsServiceFun($http, HttpService){

            var symptomsApiSerivce  = {
                all : all,
                filterService : filterService
            }

            function all(offsetValue){
                return (HttpService.PublicServiceURL('api/symptoms_list/?offset=' +offsetValue));

            }

            function filterService(filterText){
                return (HttpService.PublicServiceURL('api/symptoms_list/filter/'+filterText+"?limit=5000"));
            }

            return symptomsApiSerivce;
    }
    function causesServiceFun(CacheService, HttpService)
    {
        var causesApiSerivce  = {

                all : all,
                causeFilter : causeFilter,
                filter : filtermsp,
            }
        
        function all(params){

            return (HttpService.PublicServiceURL('api/get_conditions/'+params+"?limit=5000"));
        }

        function causeFilter(params)
        {
            return (HttpService.PublicServiceURL('api/get_conditions/'+params+"?limit=5000"));
        }
        function filtermsp(params){
            return (HttpService.PublicServiceURL('http://52.74.163.60/api/symptoms_list/msp/'+params));
        }

        return causesApiSerivce;
    }
    
    function conditionsServiceFun(HttpService)
    {
        var conditionsApiSerivce  = {

                all : all
            }


            

            function all(Params){

                return (HttpService.PublicServiceURL("api/conditions/"+Params));

            }


            return conditionsApiSerivce;
    }

    function avatarServiceFun(HttpService)
    {
        var avatarApiSerivce  = {
                all : all
            }

            function all($stateParams){
                
                return (HttpService.PublicServiceURL($stateParams));  

            }


            return avatarApiSerivce;
    }
    
    function ImageGet($http)
    {
        var vm=this;
        vm.cord={};
        vm.maleFront=function()
        {
            return "maleFront";
        };
        vm.maleBack=function()
        {
            return "maleBack";

        };
        vm.femaleFront=function()
        {
            return "femaleFront";
        };
        vm.femaleBack=function()
        {
            return "femaleBack";
        };
        
    }
})();