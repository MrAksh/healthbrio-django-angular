(function () {
  'use strict';

  angular
    .module('myApp.routes')
    .config(indexConfig);

  angular
    .module('myApp.routes')
    .config(symptomsConfig);
  
  angular
    .module('myApp.routes')
    .config(conditionsConfig);

  angular
    .module('myApp.routes')
    .config(medicationsConfig);
    
  angular
    .module('myApp.routes')
    .config(proceduresConfig);

  angular
    .module('myApp.routes')
    .config(doctorsConfig);
    
  angular
    .module('myApp.routes')
    .config(hospitalsConfig);


  angular
    .module('myApp.routes')
    .config(swaasthConfig);

  angular
      .module('myApp.routes')
      .config(searchconfig);


  indexConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  symptomsConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  conditionsConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  medicationsConfig.$inject = ['$stateProvider', '$urlRouterProvider' ];
  proceduresConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  doctorsConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  hospitalsConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  swaasthConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  searchconfig.$inject = ['$stateProvider', '$urlRouterProvider' ];
/**
   * @name config
   * @desc Define valid application routes*/

   //***  stateProvider for Symptoms*****
  window.static_path = 'http://127.0.0.1:8000/static/';

  /*window.static_path = 'amzon path';*/

  function indexConfig($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            controller:'PrimaryIndexController',
            controllerAs:'primary_vm',
            templateUrl: static_path+'templates/index/primary_index.html'
        })
        .state("secondary", {
            url: '/swaasth',
            params : {statename : null, search_text : null},
            controller:'IndexController',
            templateUrl: static_path+'templates/index/secondary_index.html'
        });

  }

  function symptomsConfig($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('symptoms',{
        url: '/symptoms/male',
        parent:'secondary',
        data: {
          'selectedTab': 0
        },
        controller: 'SymptomsController',
        controllerAs: 'symptom_vm',
        templateUrl:static_path+'templates/symptoms/symptoms.html'
    }).state('allsymptoms',{
        url: '/allsymptoms',
        parent:'secondary',
        data: {
          'selectedTab': 0
        },
        controller: 'SymptomsAllController',
        controllerAs: 'symptom_all_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_all.html'

    }).state('symptoms/male',{
        url: '/symptoms/male_front',
        parent:'secondary',
        data: {
          'selectedTab': 0
        },
        controller: 'SymptomsController_Gender_Male',
        controllerAs: 'symptom_male_vm',
        templateUrl:static_path+'templates/symptoms/male.html'
    }).state('symptoms/male_back',{
        url: '/symptoms/male_back',
        parent:'secondary',
        data: {
          'selectedTab': 0
        },
        controller: 'SymptomsController_Rotate_Male',
        controllerAs: 'symptom_male_back_vm',
        templateUrl:static_path+'templates/symptoms/male_back.html'

    }).state('symptoms/female',{
        url: '/symptoms/female_front',
        parent:'secondary',
        data: {
          'selectedTab': 0
        },
        controller: 'SymptomsController_Gender_Female',
        controllerAs: 'symptom_female_vm',
        templateUrl:static_path+'templates/symptoms/female.html'
    }).state('symptoms/female_back',{
        url: '/symptoms/female_back',
        parent:'secondary',
        data: {
          'selectedTab': 0
        },
        controller: 'SymptomsController_Rotate_Female',
        controllerAs: 'symptom_female_back_vm',
        templateUrl:static_path+'templates/symptoms/female_back.html'
    }).state('allsymptoms.causes',{
        url: '/:symptoms_id',
        params:{stmptoms_name:null, msp_enabled:null, life_threatening:null},
        controller: 'SymptomsAllCauseController',
        controllerAs: 'allsymptom_cause_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_causes.html'
    }).state('allsymptoms.causes.conditions_overview',{
        url: '/:cause_id',
        params:{title_name1:null},
        controller: 'SymptomsConditionsController',
        controllerAs: 'allsymptom_condition_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_conditions.html'
    }).state("allsymptoms.causes.conditions_overview.myconditionsoverview", {
        url: '/:myConditionId',
        params : {myConditionName:null},
        views : {
            "myConditionView" : {
                templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                controller : "MyconditionsoverviewController",
                controllerAs : "myconditionsoverview_vm"
                }
            }
    }).state('allsymptoms.causes.conditions_overview.symptomsFacilities',{
        url: '/:condition_id',
        params:{title_name2:null},
        controller: 'SymptomsFacilitiesController',
        controllerAs: 'allsymptom_facilitie_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_facilities.html'
    }).state("allsymptoms.causes.conditions_overview.medicationListFromWCIG", {
        url:"/m/:c_slug",
        controller:'WCIGMedications_Controller',
        controllerAs:'wcig_medications_vm',
        templateUrl:static_path+'templates/imgvideodialog/medicationListFromWCIG.html'
    }).state("allsymptoms.causes.conditions_overview.medicationListFromWCIG.medications_overview", {
        url:"/:medication_id",
        params:{medication_name:null},
        controller:'Medications_overview_Controller',
        controllerAs:'medications_overview_vm',
        templateUrl:static_path+'templates/medications/medications_Overview.html'
    }).state("allsymptoms.causes.conditions_overview.medicationListFromWCIG.medications_overview.mymedicationsoverview", {
        url: '/:myMedicationId',
        params : {myMedicationName:null},
        views : {
            "myMedicationView" : {
                templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                controller : "MymedicationsoverviewController",
                controllerAs : "mymedicationsoverview_vm"
                }
            }
    }).state("allsymptoms.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG", {
        url: '/s/s_list',
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("allsymptoms.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("allsymptoms.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    }).state('symptom_front',{
        url: '/:avatar_id',
        parent:'symptoms',
        params:{avatar_code:0, avatar_url:null},
        controller: 'AvatarController',
        controllerAs: 'avatar_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_of_avatar.html'
    }).state('symptom_front.causes',{
        url: '/:id',
        params:{title_name:null},
        controller: 'SymptomsAllCauseController',
        controllerAs: 'allsymptom_cause_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_causes.html'
    }).state('symptom_front.causes.conditions_overview',{
        url: '/:cause_id',
        params:{title_name1:null},
        controller: 'SymptomsConditionsController',
        controllerAs: 'allsymptom_condition_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_conditions.html'
    }).state("symptom_front.causes.conditions_overview.myconditionsoverview", {
        url: '/:myConditionId',
        params : {myConditionName:null},
        views : {
            "myConditionView" : {
                templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                controller : "MyconditionsoverviewController",
                controllerAs : "myconditionsoverview_vm"
                }
            }
    }).state('symptom_front.causes.conditions_overview.symptomsFacilities',{
        url: '/:condition_id',
        params:{title_name2:null},
        controller: 'SymptomsFacilitiesController',
        controllerAs: 'allsymptom_facilitie_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_facilities.html'
    }).state('male_front',{
        url: '/:avatar_id',
        params:{avatar_code:1, avatar_url:null},
        parent:'symptoms/male',
        controller: 'AvatarController',
        controllerAs: 'avatar_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_of_avatar.html'
    }).state('male_front.causes',{
        url: '/:stmptoms_id',
        params:{stmptoms_name:null, msp_enabled:null, life_threatening:null},
        controller: 'SymptomsAllCauseController',
        controllerAs: 'allsymptom_cause_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_causes.html'
    }).state('male_front.causes.conditions_overview',{
        url: '/:cause_id',
        params:{title_name1:null},
        controller: 'SymptomsConditionsController',
        controllerAs: 'allsymptom_condition_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_conditions.html'
    }).state("male_front.causes.conditions_overview.myconditionsoverview", {
        url: '/:myConditionId',
        params : {myConditionName:null},
        views : {
            "myConditionView" : {
                templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                controller : "MyconditionsoverviewController",
                controllerAs : "myconditionsoverview_vm"
                }
            }
    }).state('male_front.causes.conditions_overview.symptomsFacilities',{
        url: '/:condition_id',
        params:{title_name2:null},
        controller: 'SymptomsFacilitiesController',
        controllerAs: 'allsymptom_facilitie_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_facilities.html'
    }).state("male_front.causes.conditions_overview.medicationListFromWCIG", {
        url:"/m/:c_slug",
        controller:'WCIGMedications_Controller',
        controllerAs:'wcig_medications_vm',
        templateUrl:static_path+'templates/imgvideodialog/medicationListFromWCIG.html'
    }).state("male_front.causes.conditions_overview.medicationListFromWCIG.medications_overview", {
        url:"/:medication_id",
        params:{medication_name:null},
        controller:'Medications_overview_Controller',
        controllerAs:'medications_overview_vm',
        templateUrl:static_path+'templates/medications/medications_Overview.html'
    }).state("male_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.mymedicationsoverview", {
        url: '/:myMedicationId',
        params : {myMedicationName:null},
        views : {
            "myMedicationView" : {
                templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                controller : "MymedicationsoverviewController",
                controllerAs : "mymedicationsoverview_vm"
                }
            }
    }).state("male_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG", {
        url: '/s/s_list',
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("male_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("male_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    }).state('male_back',{
        url: '/:avatar_id',
        params:{avatar_code:2, avatar_url:null},
        parent:'symptoms/male_back',
        controller: 'AvatarController',
        controllerAs: 'avatar_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_of_avatar.html'
    }).state('male_back.causes',{
        url: '/:stmptoms_id',
        params:{stmptoms_name:null, msp_enabled:null, life_threatening:null},
        controller: 'SymptomsAllCauseController',
        controllerAs: 'allsymptom_cause_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_causes.html'
    }).state('male_back.causes.conditions_overview',{
        url: '/:cause_id',
        params:{title_name1:null},
        controller: 'SymptomsConditionsController',
        controllerAs: 'allsymptom_condition_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_conditions.html'
    }).state("male_back.causes.conditions_overview.myconditionsoverview", {
        url: '/:myConditionId',
        params : {myConditionName:null},
        views : {
            "myConditionView" : {
                templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                controller : "MyconditionsoverviewController",
                controllerAs : "myconditionsoverview_vm"
                }
            }
    }).state('male_back.causes.conditions_overview.symptomsFacilities',{
        url: '/:condition_id',
        params:{title_name2:null},
        controller: 'SymptomsFacilitiesController',
        controllerAs: 'allsymptom_facilitie_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_facilities.html'
    }).state("male_back.causes.conditions_overview.medicationListFromWCIG", {
        url:"/m/:c_slug",
        controller:'WCIGMedications_Controller',
        controllerAs:'wcig_medications_vm',
        templateUrl:static_path+'templates/imgvideodialog/medicationListFromWCIG.html'
    }).state("male_back.causes.conditions_overview.medicationListFromWCIG.medications_overview", {
        url:"/:medication_id",
        params:{medication_name:null},
        controller:'Medications_overview_Controller',
        controllerAs:'medications_overview_vm',
        templateUrl:static_path+'templates/medications/medications_Overview.html'
    }).state("male_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.mymedicationsoverview", {
        url: '/:myMedicationId',
        params : {myMedicationName:null},
        views : {
            "myMedicationView" : {
                templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                controller : "MymedicationsoverviewController",
                controllerAs : "mymedicationsoverview_vm"
                }
            }
    }).state("male_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG", {
        url: '/s/s_list',
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("male_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("male_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    }).state('female_front',{
        url: '/:avatar_id',
        params:{avatar_code:3, avatar_url:null},
        parent:'symptoms/female',
        controller: 'AvatarController',
        controllerAs: 'avatar_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_of_avatar.html'
    }).state('female_front.causes',{
        url: '/:stmptoms_id',
        params:{stmptoms_name:null, msp_enabled:null, life_threatening:null},
        controller: 'SymptomsAllCauseController',
        controllerAs: 'allsymptom_cause_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_causes.html'
    }).state('female_front.causes.conditions_overview',{
        url: '/:cause_id',
        params:{title_name1:null},
        controller: 'SymptomsConditionsController',
        controllerAs: 'allsymptom_condition_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_conditions.html'
    }).state("female_front.causes.conditions_overview.myconditionsoverview", {
        url: '/:myConditionId',
        params : {myConditionName:null},
        views : {
            "myConditionView" : {
                templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                controller : "MyconditionsoverviewController",
                controllerAs : "myconditionsoverview_vm"
                }
            }
    }).state('female_front.causes.conditions_overview.symptomsFacilities',{
        url: '/:condition_id',
        params:{title_name2:null},
        controller: 'SymptomsFacilitiesController',
        controllerAs: 'allsymptom_facilitie_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_facilities.html'
    }).state("female_front.causes.conditions_overview.medicationListFromWCIG", {
        url:"/m/:c_slug",
        controller:'WCIGMedications_Controller',
        controllerAs:'wcig_medications_vm',
        templateUrl:static_path+'templates/imgvideodialog/medicationListFromWCIG.html'
    }).state("female_front.causes.conditions_overview.medicationListFromWCIG.medications_overview", {
        url:"/:medication_id",
        params:{medication_name:null},
        controller:'Medications_overview_Controller',
        controllerAs:'medications_overview_vm',
        templateUrl:static_path+'templates/medications/medications_Overview.html'
    }).state("female_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.mymedicationsoverview", {
        url: '/:myMedicationId',
        params : {myMedicationName:null},
        views : {
            "myMedicationView" : {
                templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                controller : "MymedicationsoverviewController",
                controllerAs : "mymedicationsoverview_vm"
                }
            }
    }).state("female_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG", {
        url: '/s/s_list',
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("female_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("female_front.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    }).state('female_back',{
        url: '/:avatar_id',
        params:{avatar_code:4, avatar_url:null},
        parent:'symptoms/female_back',
        controller: 'AvatarController',
        controllerAs: 'avatar_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_of_avatar.html'
    }).state('female_back.causes',{
        url: '/:stmptoms_id',
        params:{stmptoms_name:null, msp_enabled:null, life_threatening:null},
        controller: 'SymptomsAllCauseController',
        controllerAs: 'allsymptom_cause_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_causes.html'
    }).state('female_back.causes.conditions_overview',{
        url: '/:cause_id',
        params:{title_name1:null},
        controller: 'SymptomsConditionsController',
        controllerAs: 'allsymptom_condition_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_conditions.html'
    }).state("female_back.causes.conditions_overview.myconditionsoverview", {
        url: '/:myConditionId',
        params : {myConditionName:null},
        views : {
            "myConditionView" : {
                templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                controller : "MyconditionsoverviewController",
                controllerAs : "myconditionsoverview_vm"
                }
            }
    }).state('female_back.causes.conditions_overview.symptomsFacilities',{
        url: '/:condition_id',
        params:{title_name2:null},
        controller: 'SymptomsFacilitiesController',
        controllerAs: 'allsymptom_facilitie_vm',
        templateUrl:static_path+'templates/symptoms/symptoms_facilities.html'
    }).state("female_back.causes.conditions_overview.medicationListFromWCIG", {
        url:"/m/:c_slug",
        controller:'WCIGMedications_Controller',
        controllerAs:'wcig_medications_vm',
        templateUrl:static_path+'templates/imgvideodialog/medicationListFromWCIG.html'
    }).state("female_back.causes.conditions_overview.medicationListFromWCIG.medications_overview", {
        url:"/:medication_id",
        params:{medication_name:null},
        controller:'Medications_overview_Controller',
        controllerAs:'medications_overview_vm',
        templateUrl:static_path+'templates/medications/medications_Overview.html'
    }).state("female_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.mymedicationsoverview", {
        url: '/:myMedicationId',
        params : {myMedicationName:null},
        views : {
            "myMedicationView" : {
                templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                controller : "MymedicationsoverviewController",
                controllerAs : "mymedicationsoverview_vm"
                }
            }
    }).state("female_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG", {
        url: '/s/s_list',
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("female_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("female_back.causes.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    });

}

//***  stateProvider for Conditions*****
function conditionsConfig($stateProvider, $urlRouterProvider){

    $stateProvider
    .state("conditions", {
        url:"/conditions",
        parent:'secondary',
        data: {
          'selectedTab': 1
        },
        controller:'ConditionsController1',
        controllerAs:'conditions_vm',
        templateUrl:static_path+'templates/conditions/conditions.html'
    }).state("conditions.conditions_overview", {
        url: '/:cause_id',
        params:{title_name1:null},
        controller: 'SymptomsConditionsController',
        controllerAs: 'allsymptom_condition_vm',
        templateUrl:static_path+'templates/conditions/symptoms_conditions.html'
    }).state("conditions.conditions_overview.myconditionsoverview", {
        url: '/:myConditionId',
        params : {myConditionName:null},
        views : {
            "myConditionView" : {
                templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                controller : "MyconditionsoverviewController",
                controllerAs : "myconditionsoverview_vm"
                }
            }
    }).state("conditions.conditions_overview.medicationListFromWCIG", {
        url:"/m/:c_slug",
        controller:'WCIGMedications_Controller',
        controllerAs:'wcig_medications_vm',
        templateUrl:static_path+'templates/imgvideodialog/medicationListFromWCIG.html'
    }).state("conditions.conditions_overview.medicationListFromWCIG.medications_overview", {
        url:"/:medication_id",
        params:{medication_name:null},
        controller:'Medications_overview_Controller',
        controllerAs:'medications_overview_vm',
        templateUrl:static_path+'templates/medications/medications_Overview.html'
    }).state("conditions.conditions_overview.medicationListFromWCIG.medications_overview.mymedicationsoverview", {
        url: '/:myMedicationId',
        params : {myMedicationName:null},
        views : {
            "myMedicationView" : {
                templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                controller : "MymedicationsoverviewController",
                controllerAs : "mymedicationsoverview_vm"
                }
            }
    }).state("conditions.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG", {
        url: '/s/s_list',
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("conditions.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("conditions.conditions_overview.medicationListFromWCIG.medications_overview.specialityListFromWCIG.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    });/*.state("conditions.symptomsConditions.further_reading", {

        url: '/rurther_reading',
        views:{
            "conditions_overview":{

                controller: 'SymptomsConditionsController',
                controllerAs: 'allsymptom_condition_vm',
                templateUrl:static_path+'templates/conditions/further_reading.html'
            }
        }
    }).state("conditions.symptomsConditions.images_videos", {

            url: '/images_videos',
            views:{
            "conditions_overview":{

                controller: 'SymptomsConditionsController',
                controllerAs: 'allsymptom_condition_vm',
                templateUrl:static_path+'templates/conditions/images_videos.html'
            }
        }
    }).state("conditions.symptomsConditions.article", {

            url: '/article',
            views:{
            "conditions_overview":{

                controller: 'SymptomsConditionsController',
                controllerAs: 'allsymptom_condition_vm',
                templateUrl:static_path+'templates/conditions/article.html'
            }
        }
    })*/;

}

//***  stateProvider for Medications*****
  function medicationsConfig($stateProvider, $urlRouterProvider){

    $stateProvider
    .state("medications", {
        url:"/medications",
        parent:'secondary',
        data: {
          'selectedTab': 2
        },
        controller:'MedicationsController',
        controllerAs:'medications_vm',
        templateUrl:static_path+'templates/medications/medications.html'
    }).state("medications.medications_overview", {
        url:"/:medication_id",
        params:{medication_name:null},
        controller:'Medications_overview_Controller',
        controllerAs:'medications_overview_vm',
        templateUrl:static_path+'templates/medications/medications_Overview.html'
    }).state("medications.medications_overview.mymedicationsoverview", {
        url: '/:myMedicationId',
        params : {myMedicationName:null},
        views : {
            "myMedicationView" : {
                templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                controller : "MymedicationsoverviewController",
                controllerAs : "mymedicationsoverview_vm"
                }
            }
    }).state("medications.medications_overview.specialityListFromWCIG", {
        url: '/s/s_list',
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("medications.medications_overview.specialityListFromWCIG.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("medications.medications_overview.specialityListFromWCIG.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    });
  }

//***  stateProvider for Procedures*****
  function proceduresConfig($stateProvider, $urlRouterProvider){

    $stateProvider
    .state("procedures", {
        url:"/procedures",
        parent:'secondary',
        data: {
          'selectedTab': 3
        },
        controller:'ProceduresController',
        controllerAs:'procedures_vm',
        templateUrl:static_path+'templates/procedures/procedures.html'
    }).state("procedures.procedures_overview", {
        url:"/:procedure_id",
        params:{procedure_name:null},
        controller:'ProceduresOverviewController',
        controllerAs:'procedures_overview_vm',
        templateUrl:static_path+'templates/procedures/procedures_Overview.html'
    }).state("procedures.procedures_overview.myproceduresoverview", {
        url: '/:myProcedureId',
        params : {myProcedureName:null},
        views : {
            "myProcedureView" : {
                templateUrl:static_path+'templates/dashboard/medical/myprocedures/my-procedures-overview.html',
                controller : "MyproceduresoverviewController",
                controllerAs : "myproceduresoverview_vm"
                }
            }
    });
  }

  //***  stateProvider for Doctors*****
  function doctorsConfig($stateProvider, $urlRouterProvider){

    $stateProvider
    .state("doctors", {
        url:"/doctors",
        params : {doctors_params : 1},
        parent:'secondary',
        data: {
          'selectedTab': 5
        },
        controller:'DoctorsController',
        controllerAs:'doctors_vm',
        templateUrl:static_path+'templates/doctors/doctors.html'
    }).state("doctors.doctor_list", {
        url:"/:specility_id",
        controller:'DoctorsOverviewController',
        controllerAs:'doctors_overview_vm',
        templateUrl:static_path+'templates/doctors/doctors_Overview.html'
    }).state("doctors.doctor_list.overview",{
        url:"/:doctor_id",
        params : {doctor_name:null},
        controller :'DoctorsDetailsController',
        controllerAs :'doctorsDetails_vm',
        templateUrl : static_path + 'templates/doctors/doctors_Details.html'
    });


  }

  //***  stateProvider for Hospitals*****
  function hospitalsConfig($stateProvider, $urlRouterProvider){

    $stateProvider
    .state("hospitals", {
        url:"/hospitals",
        parent:'secondary',
        data: {
          'selectedTab': 4
        },
        controller:'HospitalsController',
        controllerAs:'hospitals_vm',
        templateUrl:static_path+'templates/hospitals/hospitals.html'
    });
  }


  function swaasthConfig($stateProvider, $urlRouterProvider){
      $stateProvider
          .state("dashboard",{
              url:"/myswaasth",
              parent:'secondary',
              data: {
                  'selectedTab': 6
                },
              controller:'DashboardController',
              controllerAs :'dashboard_vm',
              templateUrl : static_path + 'templates/dashboard/dashboard.html'
          })
          .state("dashboard.myprofile",{
              url:"/my-profile",
              controller:'MyprofileController',
              controllerAs:'myprofile_vm',
              templateUrl:static_path + 'templates/dashboard/myprofile/my-profile.html'
          })
          .state("dashboard.myprofile.myprofileoverview",{
              url:"/myprofile-overview/:profile_id",
              controller:'MyprofileoverviewController',
              controllerAs:'myprofileoverview_vm',
              templateUrl:static_path + 'templates/dashboard/myprofile/my-profile-overview.html'
          })
          .state("dashboard.changePass",{
              url:"/change-password",
              controller : 'changePasswordController',
              controllerAs : 'changepassword_vm',
              templateUrl : static_path + 'templates/dashboard/myprofile/change-password.html'
          })
          .state("dashboard.myconditions",{
              url:"/my-conditions",
              controller:'MyconditionController',
              controllerAs:'mycondition_vm',
              templateUrl:static_path + 'templates/dashboard/medical/myconditions/my-conditions.html'
          })
          .state("dashboard.myconditions.myconditionsoverview",{
              url:"/mycondition-overview/:myConditionId",
              controller:'MyconditionsoverviewController',
              controllerAs:'myconditionsoverview_vm',
              templateUrl:static_path + 'templates/dashboard/medical/myconditions/my-conditions-overview.html'
          })
          .state("dashboard.mymedications",{
              url:"/my-medications",
              controller:'MymedicationsController',
              controllerAs:'mymedications_vm',
              templateUrl:static_path + 'templates/dashboard/medical/mymedications/my-medications.html'
          })
          .state("dashboard.mymedications.mymedicationsoverview",{
              url:"/mymedications-overview/:myMedicationId",
              controller:'MymedicationsoverviewController',
              controllerAs:'mymedicationsoverview_vm',
              templateUrl:static_path + 'templates/dashboard/medical/mymedications/my-medications-overview.html'
          })
          .state("dashboard.myprocedures",{
              url:"/my-procedures",
              controller:'MyproceduresController',
              controllerAs:'myprocedures_vm',
              templateUrl:static_path + 'templates/dashboard/medical/myprocedures/my-procedures.html'
          })
          .state("dashboard.myprocedures.myproceduresoverview",{
              url:"/myprocedures-overview/:myProcedureId",
              controller:'MyproceduresoverviewController',
              controllerAs:'myproceduresoverview_vm',
              templateUrl:static_path + 'templates/dashboard/medical/myprocedures/my-procedures-overview.html'
          })
          .state("dashboard.myallergies",{
              url:"/my-allergies",
              controller:'MyallergiesController',
              controllerAs:'myallergies_vm',
              templateUrl:static_path + 'templates/dashboard/medical/myAllergies/my-allergies.html'
          })
          .state("dashboard.myallergies.myallergiesoverview",{
              url:"/myallergy-overview/:myallergyId",
              controller:'MyallergiesoverviewController',
              controllerAs:'myallergiesoverview_vm',
              templateUrl:static_path + 'templates/dashboard/medical/myAllergies/my-allergies-overview.html'
          });
  }



  function searchconfig($stateProvider, $urlRouterProvider){
      $stateProvider
          .state("secondarysearch",{
              url:"/search?:search_text",
              parent:'secondary',
              data: {
                'selectedTab': 7
              },
              controller:'SecondarySearchController',
              controllerAs :'secondarysearch_vm',
              templateUrl : static_path + 'templates/search/search.html'
          }).state("secondarysearch.causes",{
                url: '/s/:stmptoms_id',
                params:{stmptoms_name:null, msp_enabled:null, life_threatening:null},
                controller: 'SymptomsAllCauseController',
                controllerAs: 'allsymptom_cause_vm',
                templateUrl:static_path+'templates/symptoms/symptoms_causes.html'
          }).state("secondarysearch.conditions_overview",{
                url: '/c/:cause_id',
                params:{title_name1:null},
                controller: 'SymptomsConditionsController',
                controllerAs: 'allsymptom_condition_vm',
                templateUrl:static_path+'templates/conditions/symptoms_conditions.html'
          }).state("secondarysearch.conditions_overview.myconditionsoverview", {
                url: '/:myConditionId',
                params : {myConditionName:null},
                views : {
                    "myConditionView" : {
                        templateUrl:static_path+'templates/dashboard/medical/myconditions/my-conditions-overview.html',
                        controller : "MyconditionsoverviewController",
                        controllerAs : "myconditionsoverview_vm"
                        }
                    }
           }).state("secondarysearch.procedures_overview",{
                url:"/p/:procedure_id",
                params:{procedure_name:null},
                controller:'ProceduresOverviewController',
                controllerAs:'procedures_overview_vm',
                templateUrl:static_path+'templates/procedures/procedures_Overview.html'
          }).state("secondarysearch.procedures_overview.myproceduresoverview", {
                url: '/:myProcedureId',
                params : {myProcedureName:null},
                views : {
                    "myProcedureView" : {
                        templateUrl:static_path+'templates/dashboard/medical/myprocedures/my-procedures-overview.html',
                        controller : "MyproceduresoverviewController",
                        controllerAs : "myproceduresoverview_vm"
                        }
                    }
          }).state("secondarysearch.medications_overview",{
                url:"/m/:medication_id",
                params:{medication_name:null},
                controller:'Medications_overview_Controller',
                controllerAs:'medications_overview_vm',
                templateUrl:static_path+'templates/medications/medications_Overview.html'
          }).state("secondarysearch.medications_overview.mymedicationsoverview", {
                url: '/:myMedicationId',
                params : {myMedicationName:null},
                views : {
                    "myMedicationView" : {
                        templateUrl:static_path+'templates/dashboard/medical/mymedications/my-medications-overview.html',
                        controller : "MymedicationsoverviewController",
                        controllerAs : "mymedicationsoverview_vm"
                        }
                    }
          });
}



})();