/**
 * Created by IDCS12 on 3/18/2015.
 */
 (function(){
    angular.module('myApp.WhereCouldIGo.controller')
        .controller('WhereCouldIGoController', WhereCouldIGoControllerFun);
    angular.module('myApp.WhereCouldIGo.controller')
        .controller('WCIGMedications_Controller', WCIGMedications_ControllerFun);
    
    WhereCouldIGoControllerFun.$inject = ['$scope', '$rootScope', '$window', 'CacheService'];
    WCIGMedications_ControllerFun.$inject = ['$scope', '$state', '$stateParams', '$window', 'ScrollService', 'CacheService'];

    function WhereCouldIGoControllerFun($scope, $rootScope, $window, CacheService)
    {
        var vm=this;
        $scope.isUserLocationFind = false;
        vm.where_could_go_list = [];
        vm.viewheight = {
            height : ($window.innerHeight - 171)+"px"
        }

        if(CacheService.isCache("user_location"))
        {
            $scope.isUserLocationFind = true;
        }

        //********** List Come From Condition Controller**********
        $scope.$watch('whereCouldGoData', function() {
           vm.where_could_go_list = $scope.whereCouldGoData; 
        });

        $scope.$on("LocationChangesEvent", function()
        {
            $scope.isUserLocationFind = true;
        });
        
    }


    //Medication List When Come From Condition (Where Could I GO Tab)*********
    function WCIGMedications_ControllerFun($scope, $state, $stateParams, $window, ScrollService, CacheService)
    {
        var vm = this;
        vm.wcig_medication_list = [];

        vm.viewheight = {
            height : ($window.innerHeight - 171)+"px"
        }

        if(CacheService.isCache("conditions_"+$stateParams.c_slug))
        {
            vm.wcig_medication_list = CacheService.getCache("conditions_"+$stateParams.c_slug)[0].medications;
        }

        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".medicationListFromWCIG")));
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();
    }
})();

