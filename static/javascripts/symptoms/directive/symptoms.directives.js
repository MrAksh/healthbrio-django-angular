/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';


    angular.module('myApp.symptoms.directives')
        .directive('frontRotate', frameRotateFun);


    frameRotateFun.$inject = ['$state', '$rootScope', '$location'];




    function frameRotateFun($state, $rootScope, $location){
         return{
            restrict : 'AE',

            link : function (scope, element, attrs){
               var rotateFront = document.getElementById(attrs.buttonid);
                //alert(attrs.buttonid);
                    rotateFront.onclick = function(){
                        if(attrs.buttonid == "rotateFront"){
                            //alert("rotate male front to back");
                            var statename = 'symptoms/male_back';
                            var imgurl = 'lib/image/avatar/male_character.png';
                            rotateImg(statename, imgurl);

                        }else if(attrs.buttonid == "rotateBack"){
                            //alert("rotate it front");
                            var statename = 'symptoms/male';
                            var imgurl = 'lib/image/avatar/male_character.png';
                            rotateImg(statename, imgurl);
                        }else if(attrs.buttonid == "rotateFemaleFront"){
                            //alert("rotate female front to back");
                            var statename = 'symptoms/female_back';
                            var imgurl = 'lib/image/avatar/male_character.png';
                            rotateImg(statename, imgurl);
                        }else if(attrs.buttonid == "rotateFemaleBack"){
                            //alert("rotate female back to front");
                            var statename = 'symptoms/female';
                            var imgurl = 'lib/image/avatar/male_character.png';
                            rotateImg(statename, imgurl);

                        }
               };


                function rotateImg(statename, imgurl){
                        var coin,
                            coinImage,
                            canvas;
                        var i =0;
                        function gameLoop () {
                            if (i<50){
                                 window.requestAnimationFrame (gameLoop);
                                i++;
                            }else{
                                i =0;
                            }
                          //
                          coin.update();
                          coin.render();
                        }

                        function sprite (options) {

                            var that = {},
                                frameIndex = 0,
                                tickCount = 0,
                                ticksPerFrame = options.ticksPerFrame || 0,
                                numberOfFrames = options.numberOfFrames || 1;

                            that.context = options.context;
                            that.width = options.width;
                            that.height = options.height;
                            that.image = options.image;

                            that.update = function () {

                                tickCount += 1;

                                if (tickCount > ticksPerFrame) {

                                    tickCount = 0;

                                    // If the current frame index is in range
                                    if (frameIndex < numberOfFrames - 1) {
                                        // Go to the next frame
                                        frameIndex += 1;
                                    } else {
                                        //frameIndex = 0;
                                    }
                                }
                                setTimeout(function(){

                                    $state.go(statename);

                                    //alert("sdfsd");
                                    //$location.path(statename)

                                    //
                                }, 1000);

                            };

                            that.render = function () {

                              // Clear the canvas
                              that.context.clearRect(0, 0, that.width, that.height);

                              // Draw the animation
                              that.context.drawImage(
                                that.image,
                                frameIndex * that.width / numberOfFrames,
                                25,
                                that.width / numberOfFrames +135,
                                that.height + 170,
                                0,
                                0,
                                that.width / numberOfFrames,
                                that.height);
                            };

                            return that;
                        }

                        // Get canvas
                        canvas = document.getElementById("coinAnimation");
                        canvas.style.background = "none";
                        canvas.width = 270;
                        canvas.height = 496;

                        // Create sprite sheet
                        coinImage = new Image();

                        // Create sprite
                        coin = sprite({
                            context: canvas.getContext("2d"),
                            width: 4213,
                            height: 496,
                            image: coinImage,
                            numberOfFrames: 11,
                            ticksPerFrame: 2
                        });

                        // Load sprite sheet
                        coinImage.addEventListener("load", gameLoop);
                        coinImage.src = $rootScope.static_path + imgurl;
                };


                (function() {
                    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
                    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
                    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
                    // MIT license

                    var lastTime = 0;
                    var vendors = ['ms', 'moz', 'webkit', 'o'];
                    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
                    }

                    if (!window.requestAnimationFrame)
                        window.requestAnimationFrame = function(callback, element) {
                            var currTime = new Date().getTime();
                            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                              timeToCall);
                            lastTime = currTime + timeToCall;
                            return id;
                        };

                    if (!window.cancelAnimationFrame)
                        window.cancelAnimationFrame = function(id) {
                            clearTimeout(id);
                        };
                }());



            }
        }

    }






})();
