/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var cache_service=angular.module('myApp.cacheService', []);
    cache_service.service('CacheService', CacheServiceFun);
    CacheServiceFun.$inject=['$sessionStorage'];
    function CacheServiceFun($sessionStorage)
    {
        var countSession=0;
        for(var g=0; g<window.sessionStorage.length; g++)
        {
            if(window.sessionStorage.key(g).indexOf("on_refresh_")!=-1)
            {
                countSession++;
            }
        }

        this.isCache=function(key){

            if(window.sessionStorage.getItem(key)!=null){
                return true;
            }
            else
            {
                return false;
            }
        }
        this.setCache=function(key, value)
        {
            //cache.put(key, value);
            window.sessionStorage.setItem(key, JSON.stringify(value));
        }
        this.getCache=function(key)
        {

            if((countSession--)<=1)
            {
                window.isRefresh=false;
            }
            return JSON.parse(window.sessionStorage.getItem(key));
            
        }
        this.removeAllCache=function()
        {
            for(var g=0; g<window.sessionStorage.length; g++)
            {
                if(window.sessionStorage.key(g).indexOf("on_refresh_")!=-1)
                {
                    window.sessionStorage.removeItem(window.sessionStorage.key(g));
                    g--;
                }
            }
        }
        this.removeCache=function(key)
        {
            window.sessionStorage.removeItem(key);
        }
    }

})();