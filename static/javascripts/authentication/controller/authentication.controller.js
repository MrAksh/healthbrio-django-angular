/**
 * Created by IDCS12 on 5/22/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.authentication.controller')
        .controller('FbController', FbController);



    FbController.$inject = ['$scope', '$rootScope', '$timeout','Facebook','fbloggedInStatus', 'AuthService', '$base64', '$http', 'CacheService'];


    function FbController($scope, $rootScope, $timeout, Facebook, fbloggedInStatus, AuthService, $base64, $http, CacheService){

        //alert("fb controller");
        var vm = this;
        //define user empty data
         vm.user = {};
        //define user logged status
         vm.logged = false;
         vm.userIsConnected = false;
         vm.authResponse={};


      Facebook.getLoginStatus(function(response){
        
          if(!CacheService.isCache("client_info"))
          {
            vm.userIsConnected = false;
          }
          else
          {
            vm.userIsConnected = true;
          }
      });

      vm.IntentLogin = function(){
          
          if(!vm.userIsConnected){
            vm.login();
          }
      };

      vm.login = function(){
          //alert("login callsed")
          Facebook.login(function(response){
              if(response.status === 'connected'){
                  vm.logged = true;
                  vm.me(response.authResponse);
              } else if(response.status === 'not_authorized'){
                  console.log("person is not authorized in your app");
              } else{
                  console.log("person is not an facebook user");
              }
          })
      };

      vm.logout = function(){
          //alert("logout");
          Facebook.logout(function(){
              $scope.$apply(function(){
                vm.user = {};
                vm.authResponse = {};
                vm.logged = false;
                //localStorage.removeItem("userDetails");
                localStorage.removeItem("fbToken");
              });
          });
      };

      vm.me =function( authResponse) {
          //alert("me is called on successful login");
          Facebook.api('/me', function (response) {
              //alert(response);
              //console.log(response.authResponse.accessToken);
              $scope.$apply(function () {
                  vm.user = JSON.stringify(response);
                  vm.authResponse = authResponse;
                  AuthService.sendFBToken(vm.authResponse.accessToken);
                  fbloggedInStatus.setfbStatus(true);
              });
          });
      }


      $scope.login=function()
      {
        var result=$base64.encode($scope.lmail+':'+$scope.lpass);
        $scope.isLoginButtonClick=true;
        $http({
          url:$rootScope.static_server_path+'client/login/',
          method:'GET',
          headers:{
              'Content-Type':'application/json',
              'Authorization' : 'Basic '+result
          }
        }).success(function(response){
          $scope.isLoginButtonClick=false;
          $scope.lpass="";
          AuthService.sendSwaasthToken(response);
        }).error(function(error, status){
          $scope.lpass="";
          $scope.isLoginButtonClick=false;
          if(status==401)
          {
            $scope.alerts[0]={ type: 'danger', msg: 'You Are UNAUTHORIZED Person. PLEASE TRY AGAIN' };
          }
          else
          {
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
          }
        });

      }


      //********For Submit**********
    $scope.submit=function()
    {
      $scope.isSubmitButtonClick=true;
      $http({
        url:$rootScope.static_server_path+'client/sign_up/',
        method:'POST',
        headers:{
            'Content-Type':'application/json'
        },
        data : {
          'username' : $scope.uname,
          'email' : $scope.umail,
          'password' : $scope.upass
        }
      }).success(function(response){
        $scope.isSubmitButtonClick=false;
        $scope.alerts[0]={ type: 'info', msg: "Welcome "+response.username + " Check your e-mail for Account Conformation" };
      }).error(function(error, status){
        $scope.isSubmitButtonClick=false;
        if(status==404)
        {
          $scope.alerts[0]={ type: 'danger', msg:"Connection not Found" };
        }
        else
        {
          $scope.alerts[0]={ type: 'danger', msg:"Mail-ID already registered" };
        }
      });
      $scope.upass="";
    }

    }

})();