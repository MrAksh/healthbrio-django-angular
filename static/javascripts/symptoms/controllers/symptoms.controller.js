/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    'use strict';

    window.isRefresh=false;
    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController', SymptomsController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsAllController', SymptomsAllController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Rotate_Male', SymptomsController_Rotate_Male);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Gender_Male', SymptomsController_Change_Male);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Rotate_Female', SymptomsController_Rotate_Female);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsController_Gender_Female', SymptomsController_Change_Female);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsAllCauseController', SymptomsAllCauseController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsConditionsController', ConditionsController);

    angular.module('myApp.symptoms.controller')
        .controller('SymptomsFacilitiesController', SymptomsFacilitiesController);

    angular.module('myApp.symptoms.controller')
        .controller('AvatarController', AvatarController);


    SymptomsController.$inject = ['$scope','$http', 'ScrollService', 'CacheService'];
    SymptomsAllController.$inject = ['$scope','$http', '$interval', '$timeout', '$window', 'symptomsService', 'CacheService','$mdDialog','$rootScope', 'ScrollService'];
    SymptomsController_Rotate_Male.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService'];
    SymptomsController_Rotate_Female.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService'];
    SymptomsController_Change_Male.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService'];
    SymptomsController_Change_Female.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$window', 'ImageGet', 'ScrollService'];
    SymptomsAllCauseController.$inject = ['$scope','$http', '$stateParams', '$state', '$window', 'causesService', 'ScrollService', 'CacheService'];
    ConditionsController.$inject = ['$scope','$rootScope', '$http', '$stateParams', '$state', '$location', '$window', 'ScrollService', 'conditionsService', 'CacheService','$mdDialog'];
    SymptomsFacilitiesController.$inject = ['$scope', '$http', '$stateParams', '$state', 'ScrollService', 'CacheService'];
    AvatarController.$inject = ['$scope','$http', '$stateParams', '$state', '$window','AvatarService', 'ScrollService', 'CacheService', '$mdDialog','$rootScope'];



    function SymptomsController($scope, $http, ScrollService, CacheService){

        var vm = this;
        //outerFun();
        $scope.pageClass="tempclass1";
        ScrollService.scrollBegin();
        setBodyBack(0);

    }
    function SymptomsAllController($scope, $http, $interval, $timeout, $window, symptomsService, CacheService, $mdDialog, $rootScope, ScrollService) {
        var vm = this;

        vm.isAllSymptoms = true;
        vm.symptomsList = [];
        vm.isLife_threatening=false;

        vm.isDisabled=false;

        vm.isLoading=false;
        
        vm.busy = false;

        vm.after = 0;

        vm.countover = true;
        var isloaded=true;
        vm.isFilter=false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171 - 65;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };

        /*if (CacheService.isCache("symptomsAll_cache")) {
            var data = CacheService.getCache("symptomsAll_cache");

            isloaded=false;
            var i=0;
            vm.isLoading= true;
             $timeout(function(){
                 vm.isLoading=false;

                var interval=$interval(function(){

                vm.symptomsList.push(data.list[i]);
                i++;

                    if(i==data.list.length)
                    {
                        isloaded=true;
                        $interval.cancel(interval);
                    }

                }, 10);
             }, 1500);

            vm.after = data.after;
            window.isRefresh = false;
            vm.busy = false;
        }*/

        vm.loadMore = function () {
            if (CacheService.isCache("symptomsAll_cache")) {

                var data = CacheService.getCache("symptomsAll_cache");

                if(data.after>vm.after)
                {
                    isloaded=false;
                    for(var i=vm.after; i<(vm.after+20); i++)
                    {
                        if(data.list[i]!=null)
                        {
                            vm.symptomsList.push(data.list[i]);
                        }
                    }
                    vm.after+=20;
                    if(data.after<=vm.after)
                    {
                        vm.after = data.after;
                        isloaded=true;
                    }
                    vm.busy = false;
                }
                if(vm.after>data.total)
                {
                    vm.countover=false;
                }
                vm.isLoading = false;
            }
            if (vm.busy || !isloaded) return;
            if(vm.isFilter) return;
            //var url = "http://52.74.163.60/api/symptoms_list/?offset="+vm.after;
            if (vm.countover) {
                vm.busy = true;
                symptomsService.all(vm.after).success(function (data) {
                    var items = data.results;
                    for (var i = 0; i < items.length; i++) {
                        vm.symptomsList.push(items[i]);
                    }
                    console.log(vm.after);
                    vm.after = vm.after + 20;
                    var data1 = {
                        list: vm.symptomsList,
                        after: vm.after,
                        total : data.count
                    };

                    CacheService.setCache("symptomsAll_cache", data1);
                    vm.busy = false;
                    if (vm.after >= data.count) {
                        console.log("false countover");
                        vm.countover = false;
                    } else {
                        console.log("true countover ");
                    }
                    vm.isLoading = false;
                }.bind(this)).error(function (error) {
                    vm.busy = false;
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };

                });
            }
        }

        //********************

        var timeout, interval;
        vm.filterListChange=function(filterList)
        {
            vm.isFilter=true;
            vm.symptomsList=[];

            if(filterList!="")
            {
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout=$timeout(function(){
                    vm.isDisabled=true;
                    vm.isFilter=true;
                    symptomsService.filterService(filterList).success(function (response) {
                        vm.symptomsList=response.results;
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }

            if(filterList=="")
            {
                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("symptomsAll_cache")) {
                var data = CacheService.getCache("symptomsAll_cache");
                isloaded=false;
                var i=0;
                interval=$interval(function(){
                    vm.symptomsList.push(data.list[i]);
                    i++;

                    if(i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }

                }, 1);
                vm.after = data.after;

                }
            }
        }
        //********************
         var globalcause;
        vm.clickLifeThreatening=function($event, cause)
        {

            //alert(cause.main_symptom.slug);
             globalcause = cause;
            //alert(cause.main_symptom.life_threatening);
            if(cause.main_symptom.life_threatening==true)
            {
                $mdDialog.show({
                  controller: WarningController,
                  templateUrl: $rootScope.static_path + 'templates/symptoms/allsymptoms.dialog1.warning.html',
                  targetEvent: $event
                });
                vm.isLife_threatening=true;
                vm.normalView=cause;
                vm.emergencyView="({stmptoms_id:"+cause.main_symptom.slug+", stmptoms_name:"+cause.main_symptom.name+", msp_enabled:"+cause.main_symptom.msp_enabled+", life_threatening:"+cause.main_symptom.life_threatening+"})";
                $event.preventDefault();
            }
            else
            {
                vm.isLife_threatening=false;
            }
        };
        function WarningController($scope, $mdDialog ){
            $scope.static_path = window.static_path;
            $scope.preUrl = 'allsymptoms';

            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.isLife_threatening=true;
            $scope.normalView=globalcause;
            //alert($scope.normalView.main_symptom.slug);
            //alert(globalcause.main_symptom.slug);
            $scope.emergencyView="({stmptoms_id:"+globalcause.main_symptom.slug+", stmptoms_name:"+globalcause.main_symptom.name+", msp_enabled:"+globalcause.main_symptom.msp_enabled+", life_threatening:"+globalcause.main_symptom.life_threatening+"})";
        }

        //********For Manually Path Copy for Share into Share Button******
        vm.pathCopyForShare=function(isSuccess)
        {
            if(isSuccess)
            {
                $scope.alerts[0]={ type: 'success', msg: 'Data Copy into Clipboard' };
            }
            else
            {
                $scope.alerts[0]={ type: 'danger', msg: 'Browser not support System Clipboard. Copy link Manually' };
            }
        }

        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();

    }

    function SymptomsController_Rotate_Male($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService){
        var vm =this;
        vm.hideImage = false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }

        vm.rotate = function (){
            $rootScope.avataranim ="rotate";
            vm.hideImage = true;
        };

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/male_back.png",
            isLoading : false
        }
        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/male_back.png",
            isLoading : false,
            mapArea:[
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'head', full:'head', shape:'poly',
                    coords:'645,753,796,751,796,728,793,720,832,704,871,678,909,639,934,597,952,548,975,541,991,533,1013,511,1027,488,1030,471,1028,458,1015,440,1002,430,990,429,970,442,977,418,982,362,980,328,964,257,939,208,897,155,861,125,805,98,743,84,672,85,618,99,571,122,523,162,487,209,460,267,447,328,445,387,449,421,455,440,446,433,430,430,419,434,406,448,399,470,406,495,422,518,437,532,457,542,477,547,486,573,504,613,528,648,558,678,591,701,622,716,650,724'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'646.9999833816338,751.9999949597128,797.9999833816338,749.9999949597128,797.9999833816338,758.9999949597128,856.9999833816338,795.9999949597128,905.9999833816338,803.9999949597128,533.9999833816338,804.9999949597128,574.9999833816338,799.9999949597128,643.9999833816338,762.9999949597128'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'533.9999833816338,804.9999636382108,541.9999833816338,818.9999636382108,525.9999833816338,844.9999636382108,509.9999833816338,868.9999636382108,486.9999833816338,899.9999636382108,468.9999833816338,916.9999636382108,448.9999833816338,927.9999636382108,432.9999833816338,929.9999636382108,427.9999833816338,926.9999636382108,422.9999833816338,897.9999636382108,427.9999833816338,873.9999636382108,440.9999833816338,843.9999636382108,468.9999833816338,816.9999636382108,499.9999833816338,809.9999636382108'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'905.9999833816338,802.9999636382108,898.9999833816338,816.9999636382108,908.9999833816338,838.9999636382108,929.9999833816338,870.9999636382108,947.9999833816338,894.9999636382108,965.9999833816338,912.9999636382108,983.9999833816338,923.9999636382108,998.9999833816338,928.9999636382108,1011.9999833816338,928.9999636382108,1014.9999833816338,905.9999636382108,1014.9999833816338,888.9999636382108,1008.9999833816338,863.9999636382108,995.9999833816338,839.9999636382108,970.9999833816338,815.9999636382108,945.9999833816338,806.9999636382108'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'back', full:'back', shape:'poly',
                    coords:'533.9999833816338,804.9999636382108,542.9999833816338,817.9999636382108,510.9999833816338,867.9999636382108,510.9999833816338,992.9999636382108,521.9999833816338,1057.9999620303631,542.9999833816338,1127.9999620303631,552.9999833816338,1152.9999620303631,890.9999833816338,1159.9999620303631,902.9999833816338,1124.9999620303631,922.9999833816338,1057.9999620303631,930.9999833816338,998.9999620303631,930.9999833816338,870.9999933518651,897.9999833816338,818.9999933518651,907.9999833816338,801.9999933518651'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'426.9999833816338,930.0000171227884,434.9999833816338,931.0000171227884,446.9999833816338,928.0000171227884,464.9999833816338,920.0000171227884,480.9999833816338,906.0000171227884,495.9999833816338,889.0000171227884,509.9999833816338,870.0000171227884,510.9999833816338,916.0000171227884,508.9999833816338,999.0000171227884,478.9999833816338,1091.9999858012866,468.9999833816338,1137.0000155149407,466.9999833816338,1159.0000155149407,459.9999833816338,1176.000013907093,429.9999833816338,1228.000013907093,391.9999833816338,1285.000013907093,353.9999833816338,1342.000013907093,317.0000043033568,1394.000013907093,264.0000043033568,1369.000013907093,280.0000043033568,1342.000013907093,306.0000043033568,1262.0000452285951,331.0000043033568,1182.0000452285951,346.0000043033568,1150.0000155149407,375.0000043033568,1112.0000155149407,387.0000043033568,1077.0000155149407,391.0000043033568,1041.0000171227884,394.0000043033568,1001.0000171227884,405.0000043033568,973.0000171227884'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'930.0000043033567,872.0000129140109,953.0000043033567,902.0000129140109,971.0000043033567,918.0000129140109,995.0000043033567,928.0000129140109,1012.0000043033567,930.0000129140109,1028.0000043033567,956.0000129140109,1041.0000043033567,985.9999815925089,1048.0000043033567,1022.9999815925089,1048.0000043033567,1057.999981592509,1057.0000043033567,1092.0000113061633,1090.0000043033567,1146.0000113061633,1116.0000043033567,1200.9999799846612,1140.0000043033567,1279.9999799846612,1156.0000043033567,1319.9999486631593,1160.0000043033567,1341.9999486631593,1175.0000043033567,1366.9999486631593,1120.0000043033567,1388.9999486631593,1092.0000043033567,1348.9999486631593,1031.0000043033567,1258.9999799846612,978.0000043033567,1172.0000113061633,964.0000043033567,1098.999981592509,945.0000043033567,1046.0000129140108,931.0000043033567,999.0000129140109'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'317,1395.0000082828371,315,1454.0000082828371,307,1484.0000082828371,288,1546.0000082828371,278,1560.0000082828371,271,1560.0000082828371,255,1594.0000082828371,248,1598.0000082828371,241,1597.0000082828371,234,1600.0000082828371,227,1600.0000082828371,220,1590.0000082828371,210,1583.0000082828371,214,1455.0000082828371,191,1477.0000082828371,180,1480.0000082828371,173,1474.0000082828371,175,1466.0000082828371,200,1432.0000082828371,217,1401.0000082828371,264,1368.0000082828371'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'1120,1388,1175,1369,1198,1384,1220,1398,1231,1412,1237,1427,1263,1462,1267,1471,1259,1476,1246,1476,1226,1452,1227,1516,1231,1579,1229,1585,1225,1588,1220,1588,1216,1593,1212,1598,1206,1598,1199,1595,1198,1593,1195,1596,1190,1596,1184,1592,1175,1574,1169,1556,1163,1558,1159,1557,1154,1550,1144,1528,1137,1492,1128,1472,1123,1447,1123,1404'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'lower-back', full:'lower-back', shape:'poly',
                    coords:'891.9999833816338,1160.9999586885622,553.9999833816338,1153.9999586885622,567.9999833816338,1299.9999586885622,545.9999833816338,1371.9999817186144,541.9999833816338,1391.9999817186144,572.9999833816338,1405.9999817186144,602.9999833816338,1415.9999817186144,656.9999833816338,1428.9999817186144,718.9999833816338,1434.9999817186144,782.9999833816338,1428.9999817186144,842.9999833816338,1411.9999817186144,900.9999833816338,1389.9999817186144,898.9999833816338,1369.9999817186144,877.9999833816338,1309.9999817186144'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'buttockrectum', full:'buttockrectum', shape:'poly',
                    coords:'600,1640.9999351108258,653,1640.9999351108258,690,1626.9999351108258,711,1596.9999351108258,720,1569.9999351108258,731,1596.9999351108258,749,1622.9999351108258,794,1635.9999351108258,848,1637.9999351108258,839,1524.9999368415424,843,1411.9999368415424,764,1429.9999368415424,719,1432.9999368415424,666,1428.9999368415424,602,1416.9999368415424'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'541.9999833816338,1391.9999817186144,603.9999833816338,1416.9999817186144,606.9999833816338,1527.9999817186144,599.9999833816338,1640.9999503971126,510.9999833816338,1637.9999503971126,514.9999833816338,1543.9999503971126,530.9999833816338,1468.9999503971126,550.9999833816338,1415.9999503971126,543.9999833816338,1405.9999503971126'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'843,1413,900,1391,902,1406,896,1411,905,1443,921,1498,931,1562,933,1641,848,1640,842,1584,838,1521,839,1472,841,1443'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'510,1638.9999351108258,657,1640.9999351108258,692,1623.9999351108258,711,1596.9999351108258,720,1562.9999351108258,717,1638.9999351108258,714,1764.9999351108258,705,1947.9999333801093,689,2095.9999299186766,696,2209.9999299186766,698,2276.99992818796,684,2405.99992818796,680,2544.99992818796,687,2641.99992818796,596,2646.99992818796,585,2494.99992818796,555,2393.99992818796,543,2263.9999299186766,548,2191.9999299186766,557,2087.999931649393,540,1941.9999333801093,519,1809.9999351108258'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'756,2646.9999299186766,852,2640.9999299186766,854,2571.9999299186766,869,2465.9999299186766,903,2279.9999299186766,899,2204.9999299186766,888,2134.9999299186766,900,1964.999931649393,923,1848.9999351108258,932,1641.9999385722588,822,1640.9999385722588,824,1634.999934591611,794,1635.9999385722588,750,1620.9999385722588,732,1601.9999385722588,722,1578.9999385722588,731,1778.9999351108258,736,1903.9999755859376,743,1959.9999755859376,752,2031.9999755859376,754,2083.9999755859376,750,2185.9999755859376,744,2233.9999755859376,754,2317.00009765625,763,2460.00009765625,764,2560.00009765625'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'597,2646,689,2640,699,2685,699,2698,694,2711,689,2754,681,2774,664,2791,640,2795,609,2783,561,2756,526,2726,527,2720,539,2714,565,2709,591,2694,596,2671'
                },
                {
                    stateName:'male_back', avatarCode:2, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'754.0000261579241,2646.000069754464,852.0000261579241,2642.000069754464,854.0000261579241,2687.000069754464,861.0000261579241,2701.000069754464,876.0000261579241,2708.000069754464,891.0000261579241,2711.000069754464,906.0000261579241,2711.000069754464,915.0000261579241,2714.000069754464,920.0000261579241,2721.000069754464,918.0000261579241,2728.000069754464,910.0000261579241,2738.000069754464,897.0000261579241,2745.000069754464,865.0000261579241,2760.000069754464,834.0000261579241,2775.000069754464,798.0000261579241,2786.000069754464,778.0000261579241,2790.000069754464,763.0000261579241,2784.000069754464,753.0000261579241,2757.000069754464,754.0000261579241,2730.000069754464,750.0000261579241,2709.000069754464,745.0000261579241,2692.000069754464,747.0000261579241,2674.000069754464,752.0000261579241,2658.000069754464'
                }

            ]
        };
        $scope.name = "rotate";

        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();

    }
    function SymptomsController_Change_Male($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService){

        var vm =this;
        vm.hideImage = false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }
        //alert(vm.isSlide);
        vm.rotate = function (){
            $rootScope.avataranim ="rotate";
            vm.hideImage = true;
        };

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/male_front.png",
            isLoading : false,
            mapArea:[
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'head', full:'head', shape:'poly',
                    coords:'159,81,138,73,133,59,128,58,124,46,125,37,130,37,133,24,140,8,157,0,169,2,182,11,187,27,188,39,192,38,193,46,191,59,186,59,181,69,172,77,168,79'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'neck', full:'neck', shape:'poly',
                    coords:'138,74,160,82,180,72,182,88,201,100,159,110,117,101,132,94,140,84'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'117,101,89,118,75,143,58,164,62,117,74,106,92,101'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'shoulder', full:'shoulder', shape:'poly',
                    coords:'202,100,232,117,244,142,260,162,261,138,255,114,239,103'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'chest', full:'chest', shape:'poly',
                    coords:'117,101,89,117,87,135,97,152,124,161,199,158,225,147,236,130,230,116,201,102'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'86,125.00000108991351,58,162.0000010899135,52,198.0000010899135,32,262.0000010899135,22,293.0000010899135,49,303.0000010899135,56,280.0000010899135,77,236.0000010899135,86,206.0000010899135,95,173.0000010899135'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'930,870,928,996,968,1130,988,1197,1076,1324,1120,1385,1169,1362,1124,1216,1080,1132,1048,1070,1049,1016,1041,978,1012,929,985,925,958,910'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'47,303,37,350,32,352,28,358,24,357,27,332,20,356,16,357,18,332,17,329,10,352,7,353,4,350,12,322,4,333,1,333,2,327,7,314,21,294'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hands', full:'hands', shape:'poly',
                    coords:'270,301,297,292,314,318,318,334,308,322,315,352,312,353,308,348,302,331,301,332,303,346,305,356,301,357,295,345,293,332,292,332,296,355,294,358,290,356,283,334,287,352,285,352,278,337,274,324'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'abdomen', full:'abdomen', shape:'poly',
                    coords:'93,146.0000010899135,94,171.0000010899135,106,197.0000010899135,112,231.0000010899135,106,251.0000010899135,109,258.0000010899135,120,274.0000010899135,138,284.0000010899135,158,292.0000010899135,182,282.0000010899135,202,273.0000010899135,212,254.0000010899135,211,245.0000010899135,209,239.0000010899135,214,192.0000010899135,224,173.0000010899135,223,152.0000010899135,187,159.0000010899135,129,158.0000010899135,110,154.0000010899135'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'108,260.000004359654,124,277.000004359654,123,340.000004359654,96,337.000004359654'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'hips', full:'hips', shape:'poly',
                    coords:'189,280.000004359654,194,338.000004359654,223,337.000004359654,213,267.000004359654,200,276.000004359654'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'pelvis', full:'pelvis', shape:'poly',
                    coords:'124,278.000004359654,122,339.000004359654,162,343.000004359654,195,340.000004359654,191,280.000004359654,160,292.000004359654,142,287.000004359654'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'97,345.00001307896207,157,344.00001307896207,148,412.00001307896207,145,474.00001307896207,144,492.00001307896207,148,530.0000130789621,141,564.000008719308,146,613.000008719308,124,617.000008719308,113,568.000008719308,103,528.000008719308,109,498.000008719308,112,472.000008719308,110,447.000008719308,101,400.000008719308'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'legs', full:'legs', shape:'poly',
                    coords:'160,343.00001307896207,222,336.00001307896207,214,428.00001307896207,208,452.00001307896207,208,482.00001307896207,215,523.0000130789621,197,619.0000130789621,172,620.0000130789621,176,600.0000130789621,179,570.0000130789621,172,529.0000130789621,177,493.00001307896207,174,461.00001307896207,173,417.00001307896207'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'122,617.0000305175781,148,620.0000305175781,149,662.0000305175781,138,670.0000305175781,118,664.0000305175781,114,662.0000305175781,113,653.0000305175781,120,641.0000305175781'
                },
                {
                    stateName:'male_front', avatarCode:1, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'173,618.000008719308,197,618.000008719308,201,634.000008719308,206,654.000008719308,206,663.000008719308,198,668.000008719308,187,667.000008719308,182,667.000008719308,175,668.000008719308,171,654.000008719308'
                }
            ]

        };

        $scope.name="ajay";

        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();
    }
    function SymptomsController_Rotate_Female($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService){
        var vm =this;
        vm.hideImage = false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }

        vm.rotate = function (){
            $rootScope.avataranim ="rotate";
            vm.hideImage = true;
        };

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/female_back.png",
            isLoading : false,
            mapArea:[
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'head', full:'head', shape:'poly',
                    coords:'603,362,571,330,556,277,554,230,579,126,625,33,695,1,739,2,777,2,818,11,856,37,887,101,910,167,923,260,903,336,826,387,774,405,731,408,693,400,650,384'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'back', full:'back', shape:'poly',
                    coords:'501,471.00000089480534,500,620.0000008948053,450,667.0000008948053,464,773.0000008948053,480,803.0000017896107,497,924.0000017896107,983,925.0000017896107,1005,798.0000017896107,1016,780.0000017896107,1033,666.0000017896107,986,622.0000008948053,981,470.00000089480534'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'583,2740,578,2765,567,2780,555,2793,543,2804,533,2805,524,2807,517,2808,509,2811,502,2812,497,2817,494,2829,503,2838,519,2842,537,2851,555,2862,574,2870,589,2879,624,2890,654,2887,670,2876,670,2855,672,2834,681,2819,680,2795,680,2769,681,2757,677,2743'
                },
                {
                    stateName:'female_back', avatarCode:4, avatarkey:'feet', full:'feet', shape:'poly',
                    coords:'794,2755,797,2799,800,2823,806,2840,808,2873,819,2887,842,2893,873,2887,904,2868,935,2854,982,2830,977,2811,957,2810,946,2804,934,2807,921,2794,901,2773,895,2755,893,2750'
                }
            ]
        };

        $scope.name= "female Back";


        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();
    }
    function SymptomsController_Change_Female($scope, $rootScope, $http, $timeout, $window, ImageGet, ScrollService){
        var vm =this;
        vm.hideImage = false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        if($scope.avataranim == 'rotate'){
            vm.isSlide = false;
        }else{
            vm.isSlide = true;
        }

        vm.rotate = function (){
            $rootScope.avataranim ="rotate";
            vm.hideImage = true;
        };

        vm.change = function(){
            $rootScope.avataranim = "change";
        };

        $scope.photo={
            src : $scope.static_path+"lib/image/avatar/female.png",
            isLoading : false,
            mapArea:[
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'head', full:'head', shape:'poly',
                    coords:'661,388,612,363,576,322,566,297,563,264,570,202,578,142,593,103,620,55,658,17,704,0,721,1,742,5,764,0,786,1,812,11,844,34,874,67,894,104,910,151,916,197,922,252,918,301,898,334,853.0000094307792,375,824.0000094307792,385,834.0000094307792,322,808.0000094307792,350,766.0000094307792,381,737.0000094307792,387,700.0000094307792,368,671.0000094307792,345,655.0000094307792,327'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'481,695,389,696,389,726,371,764,357,796,345,843,337,877,302,967,246,1051,205,1154,180,1219,287,1256,424,972,498,822,511,737'
                },
                {
                    stateName:'female_front', avatarCode:3, avatarkey:'arms', full:'arms', shape:'poly',
                    coords:'974,727,975,783,991,840,1033,906,1065,982,1072,1028,1104,1080,1140,1163,1283,1162,1261,1094,1225,1026,1175,949,1125,783,1095,727'
                }
            ]
        };
        $scope.name="female Front";
        $rootScope.clickOnTabs(false);
        ScrollService.scrollBegin();
    }


    function SymptomsAllCauseController($scope, $http, $stateParams, $state, $window, causesService, ScrollService, CacheService)
    {
      
        var vm = this;
        vm.title=$stateParams.stmptoms_name;
        vm.causesList = [];
        vm.isLoading=true;
        $scope.filter_by_gender=true;
        $scope.filter_by_age=2;
        var filter="";
        vm.age="0-5";
        vm.isNotCause=false;
        var ismsp_enabled=$stateParams.msp_enabled;
        vm.windowHeight = $window.innerHeight;
        
        if(ismsp_enabled == true){
            vm.viewheight ={
                'height': (vm.windowHeight - 371) + "px"
            };
        }else{
            vm.viewheight ={
                'height': (vm.windowHeight - 171) + "px"
            };
        }


        /*causesService.filter($stateParams.stmptoms_id).success(function(response){
            console.log(JSON.stringify(response));
        }).error(function(error, status){
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN ' + error  + " " + status};
        });*/

        //vm.isLife_threatening=false;

        /*if($stateParams.life_threatening==true)
        {
            vm.isLife_threatening=$stateParams.life_threatening;
        }*/

        if(CacheService.isCache("myAge"))
        {
            $scope.filter_by_age=CacheService.getCache("myAge");
            if($scope.filter_by_age==1)
            {
                vm.age="0-5";
            }
            else if($scope.filter_by_age==2)
            {
                vm.age="6-17";
            }
            else if($scope.filter_by_age==3)
            {
                vm.age="18-59";
            }
            else if($scope.filter_by_age==4)
            {
                vm.age="60+";
            }
        }
        if(CacheService.isCache("myGender"))
        {
            $scope.filter_by_gender=CacheService.getCache("myGender");
        }
        filter=$stateParams.stmptoms_id+"/"+($scope.filter_by_gender?"female":"male")+"/"+vm.age;

        if(ismsp_enabled==null)
        {
            if(CacheService.isCache("symptoms_"+filter))
            {
                ismsp_enabled=CacheService.getCache("symptoms_"+filter).msp_enabled;
            }
            else
            {
                ismsp_enabled=false;
            }

        }

        if(ismsp_enabled === true){
            vm.viewheight ={
            'height': (vm.windowHeight - 371) + "px"
            };
        }else{
            vm.viewheight ={
                'height': (vm.windowHeight - 171) + "px"
            };
        }



        if(ismsp_enabled==true)
        {
            if(CacheService.isCache("symptoms_"+filter))
            {
                //***Search sessionStorage when click any Cause
                vm.causesList=CacheService.getCache("symptoms_"+filter);
                ismsp_enabled=vm.causesList.msp_enabled;
                vm.title=vm.causesList.name;
                vm.isLoading=false;
            }
        }
        else
        {
            ismsp_enabled=false;
            if(CacheService.isCache("symptoms_"+$stateParams.stmptoms_id))
            {
                //***Search sessionStorage when click any Cause
                vm.causesList=CacheService.getCache("symptoms_"+$stateParams.stmptoms_id);
                ismsp_enabled=vm.causesList.msp_enabled;
                vm.title=vm.causesList.name;
                vm.isLoading=false;

            }
            else
            {
                causesService.all($stateParams.stmptoms_id).success(function(response){
                    response.msp_enabled=$stateParams.msp_enabled;
                    vm.causesList = response;
                    response.name=$stateParams.stmptoms_name;
                    CacheService.setCache("symptoms_"+$stateParams.stmptoms_id, response);
                    vm.isLoading=false;
                  }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        }

        $scope.$watch("filter_by_age", function(){

            CacheService.setCache("myAge", $scope.filter_by_age);
            if($scope.filter_by_age==1)
            {
                vm.age="0-5";
            }
            else if($scope.filter_by_age==2)
            {
                vm.age="6-17";
            }
            else if($scope.filter_by_age==3)
            {
                vm.age="18-59";
            }
            else if($scope.filter_by_age==4)
            {
                vm.age="60+";
            }

            filter=$stateParams.stmptoms_id+"/"+($scope.filter_by_gender?"female":"male")+"/"+vm.age;
            if(ismsp_enabled)
            {
                getFilter(filter);
            }
        });
        $scope.$watch("filter_by_gender", function(){

            CacheService.setCache("myGender", $scope.filter_by_gender);
            filter=$stateParams.stmptoms_id+"/"+($scope.filter_by_gender?"female":"male")+"/"+vm.age;
            if(ismsp_enabled)
            {
                getFilter(filter);
            }
        });

        function getFilter(filter)
        {
            vm.isNotCause=false;
            //vm.causesList=[];
            if(CacheService.isCache("symptoms_"+filter))
            {
                //***Search sessionStorage when click any Cause
                vm.causesList=CacheService.getCache("symptoms_"+filter);
                if(vm.causesList.results.length==0)
                {
                    vm.isNotCause=true;
                }
                ismsp_enabled=vm.causesList.msp_enabled;
                vm.title=vm.causesList.name;
                vm.isLoading=false;
            }
            else
            {
                vm.isLoading=true;
                causesService.all(filter).success(function(response){
                    if(response.results.length==0)
                    {
                        vm.isNotCause=true;
                    }
                    response.msp_enabled=true;
                    vm.causesList = response;
                    response.name=$stateParams.stmptoms_name;
                    CacheService.setCache("symptoms_"+filter, response);
                    vm.isLoading=false;
                }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        }

        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".causes")));
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();

    }

    function ConditionsController($scope, $rootScope, $http, $stateParams, $state, $location, $window, ScrollService, conditionsService, CacheService , $mdDialog)
    {
        var vm = this;
        $scope.condition_overview_menu=1;
        vm.isOverviewTab=false;
        vm.isFurtherReadingTab=false;
        vm.isImagesVideosTab=false;
        vm.isHealthArticle=false;

        vm.isImageViewerShow=false;
        vm.isVideoViewerShow=false;
        vm.imageViewerPath="";
        vm.imageViewerTitle="";
        vm.isArticleViewerShow=false;
        vm.articleViewerTitle="";
        vm.articleViewerDesc="";

        vm.conditionsOveriew=[];
        vm.isLoading=true;

        vm.myConditionId=0;
        vm.isStarFill=false;

        vm.viewheight = {
            height : ($window.innerHeight-171)+"px"
        }

        $scope.whereCouldGoData = [];

        if(CacheService.isCache("conditions_"+$stateParams.cause_id))
        {
            //***Search sessionStorage when click any Cause
            vm.conditionsOveriew=CacheService.getCache("conditions_"+$stateParams.cause_id);
            vm.title=vm.conditionsOveriew[0].name;
            vm.isLoading=false;
            //********* For Where Could Go Panel**********
            $scope.whereCouldGoData = [{
                    condition_slug : vm.conditionsOveriew[0].slug
                },
                {
                    specialties : vm.conditionsOveriew[0].medical_specialties
                },
                {
                    medications : vm.conditionsOveriew[0].medications
                },
                {
                    facility : vm.conditionsOveriew[0].medical_facility_categories
                }];
            //************************
        }
        else
        {
            conditionsService.all($stateParams.cause_id).success(function(response){
                    vm.conditionsOveriew = response;
                    vm.title=vm.conditionsOveriew[0].name;
                    CacheService.setCache("conditions_"+$stateParams.cause_id+"", vm.conditionsOveriew);
                    vm.isLoading=false;
                    //********* For Where Could Go Panel**********
                    $scope.whereCouldGoData = [{
                        condition_slug : vm.conditionsOveriew[0].slug
                    },
                    {
                        specialties : vm.conditionsOveriew[0].medical_specialties
                    },
                    {
                        medications : vm.conditionsOveriew[0].medications
                    },
                    {
                        facility : vm.conditionsOveriew[0].medical_facility_categories
                    }];
                    //************************
                }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            });
        }


        $scope.$watch("condition_overview_menu", function(){

            if($scope.condition_overview_menu==1)
            {
                //$state.go("conditions.symptomsConditions");
                vm.isOverviewTab=true;
                vm.isFurtherReadingTab=false;
                vm.isImagesVideosTab=false;
                vm.isHealthArticle=false;
                ScrollService.scrollTop(".scroll_top");
            }
            if($scope.condition_overview_menu==2)
            {
                //$state.go("conditions.symptomsConditions.rurther_reading");
                vm.isFurtherReadingTab=true;
                vm.isOverviewTab=false;
                vm.isImagesVideosTab=false;
                vm.isHealthArticle=false;
                ScrollService.scrollTop(".scroll_top");
            }
            if($scope.condition_overview_menu==3)
            {
                //$state.go("conditions.symptomsConditions.images_videos");
                vm.isImagesVideosTab=true;
                vm.isOverviewTab=false;
                vm.isFurtherReadingTab=false;
                vm.isHealthArticle=false;
                ScrollService.scrollTop(".scroll_top");
            }
            if($scope.condition_overview_menu==4)
            {
                //$state.go("conditions.symptomsConditions.article");
                vm.isHealthArticle=true;
                vm.isOverviewTab=false;
                vm.isFurtherReadingTab=false;
                vm.isImagesVideosTab=false;
                ScrollService.scrollTop(".scroll_top");
            }


        });

        vm.showImage = function(ev, index) {

            vm.index=index;
            $mdDialog.show({
              controller: DialogController,
              templateUrl: $rootScope.static_path +'templates/imgvideodialog/dialog1.img.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
          };

        function DialogController($scope, $mdDialog) {
           // Set of Photos

            $scope.static_path = window.static_path;
            $scope.photos = [];
            for(var n=0; n<vm.conditionsOveriew[0].images.length; n++)
            {
                $scope.photos.push({src: vm.conditionsOveriew[0].images[n].desktop_url, desc: vm.conditionsOveriew[0].images[n].title, isLoading:false});
            }


            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.photos.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                $scope._Index = ($scope._Index < $scope.photos.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };

        }

        vm.showVideo = function(ev, index) {
            vm.index=index;
            $mdDialog.show({
              controller: VideoController,
              templateUrl: $rootScope.static_path+ 'templates/imgvideodialog/dialog1.video.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
        };

        function VideoController($scope, $mdDialog) {
           // Set of Photos
            $scope.static_path = window.static_path;
            $scope.videos = [];

            for(var n=0; n<vm.conditionsOveriew[0].videos.length; n++)
            {
                $scope.videos.push({src: vm.conditionsOveriew[0].videos[n].player_links, desc: vm.conditionsOveriew[0].videos[n].title});
                /*$scope.videosthumbs.push({src: vm.proceduresOveriew[0].video[n].thumbnail_url, desc: vm.proceduresOveriew[0].video[n].title});*/
            }


            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                alert("prev");
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.videos.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                alert("next");
                $scope._Index = ($scope._Index < $scope.videos.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };

        }


        vm.showArticles = function(ev, index) {
            vm.index=index;
            $mdDialog.show({
              controller: articleController,
              templateUrl: $rootScope.static_path+ 'templates/imgvideodialog/dialog1.articles.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
        };

        function articleController($scope, $mdDialog){
            // Set of Photos
            vm.isArticleViewerShow=true;
            /*vm.articleViewerTitle=article.title;
            vm.articleViewerDesc=article.article_body;*/
            $scope.static_path = window.static_path;
            $scope.articles = [];
            for(var n=0; n<vm.conditionsOveriew[0].article.length; n++)
            {
                $scope.articles.push({src: vm.conditionsOveriew[0].article[n].article_body, slug : vm.conditionsOveriew[0].article[n].slug, desc:vm.conditionsOveriew[0].article[n].title});

            }


            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                alert("prev");
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.articles.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                alert("next");
                $scope._Index = ($scope._Index < $scope.articles.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };
        }

        //****** Check Condition already fill or not*******
        if(CacheService.isCache("myConditionList"))
        {
            var myConditionList=CacheService.getCache("myConditionList");
            for(var m=0; m<myConditionList.length; m++)
            {
                if(myConditionList[m].condition==vm.title)
                {
                    vm.isStarFill=true;
                    vm.myConditionId=myConditionList[m].id;
                }
            }
        }

        vm.closeView=function(){

            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".conditions_overview")));
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();
    }

    function SymptomsFacilitiesController($scope, $http, $stateParams, $state, ScrollService, CacheService)
    {
        var vm = this;
        vm.title=$stateParams.title_name2;
        vm.isLoading=true;
        /*vm.title=$stateParams.title_name1;
        vm.conditionList=[];
        conditionsService.all($stateParams).success(function(response){
            vm.conditionList = response;
            }).error(function(){
                alert("error")
            });*/

        vm.closeView=function(){

            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".symptomsFacilities")));
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();
    }

    function AvatarController($scope, $http, $stateParams, $state, $window, AvatarService, ScrollService, CacheService, $mdDialog ,$rootScope)
    {
        var vms=this;
        vms.isLoading=true;
        vms.title=$stateParams.avatar_id;
        vms.rotateURL="";
        //AvatarService.all($stateParams);
        vms.causesList = [];
        vms.isLife_threatening=false;
        vms.windowHeight = $window.innerHeight;
        vms.windowHeight = vms.windowHeight - 171;

        vms.viewheight = {
            'height': vms.windowHeight + "px"
        };
        var cacheKey=$stateParams.avatar_code+"/"+$stateParams.avatar_id;

        var avatar_url_="";
        if($stateParams.avatar_code == 1 || $stateParams.avatar_code == 2)
        {
            avatar_url_="api/symptoms_list/male/"+$stateParams.avatar_id+"/";
        }
        else if($stateParams.avatar_code == 3 || $stateParams.avatar_code == 4)
        {
            avatar_url_="api/symptoms_list/female/"+$stateParams.avatar_id+"/";
        }

        if(CacheService.isCache(cacheKey))
        {
            //alert("from cache");
            //***Search sessionStorage when click any Cause
            vms.causesList=CacheService.getCache(cacheKey);
            vms.isLoading=false;
        }
        else
        {
            //alert("from api");
            //***get Data from API and store it sessionStorage
            AvatarService.all(avatar_url_).success(function(response){
                    vms.causesList = response;
                    CacheService.setCache(cacheKey, vms.causesList);
                    vms.isLoading=false;
                }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            });
        }

        var globalcause;
        vms.clickLifeThreatening=function($event, cause)
        {

            globalcause = cause;
            if(cause.life_threatening==true)
            {
                $mdDialog.show({
                  controller: WarningController,
                  templateUrl: $rootScope.static_path + 'templates/symptoms/dialog1.warning.html',
                  targetEvent: $event
                });
                vms.isLife_threatening=true;
                vms.normalView=cause;
                vms.emergencyView="({stmptoms_id:"+cause.slug+", stmptoms_name:"+cause.name+", msp_enabled:"+cause.msp_enabled+", life_threatening:"+cause.life_threatening+"})";
                $event.preventDefault();
            }
            else
            {
                vms.isLife_threatening=false;
            }
        };
        function WarningController($scope, $mdDialog ){
           $scope.static_path = window.static_path;
           // alert($stateParams.avatar_code);
            if($stateParams.avatar_code==1)
            {
                $scope.preUrl = 'male_front';
            }
            if($stateParams.avatar_code==2)
            {
                $scope.preUrl = 'male_back';
            }
            if($stateParams.avatar_code==3)
            {
                $scope.preUrl = 'female';
            }
            if($stateParams.avatar_code==4)
            {
                $scope.preUrl = 'female_back';
            }

            $scope.hide = function() {
                $mdDialog.hide();
              };
            $scope.isLife_threatening=true;
            $scope.normalView=globalcause;

            $scope.emergencyView="({stmptoms_id:"+globalcause.slug+", stmptoms_name:"+globalcause.name+", msp_enabled:"+globalcause.msp_enabled+", life_threatening:"+globalcause.life_threatening+"})";
        }


        vms.closeView=function(){
            if($stateParams.avatar_code==0)
            {
                $state.go("symptoms");
            }
            if($stateParams.avatar_code==1)
            {
                $state.go("symptoms/male");
            }
            if($stateParams.avatar_code==2)
            {
                $state.go("symptoms/male_back");
            }
            if($stateParams.avatar_code==3)
            {
                $state.go("symptoms/female");
            }
            if($stateParams.avatar_code==4)
            {
                $state.go("symptoms/female_back");
            }
            ScrollService.scrollBack();
        }
        ScrollService.scrollStart();
    };
})();



function outerFun()
{
    $('#avatar_img').mapster({
    mapKey: 'avatar-key',
    singleSelect: true,
    onClick:function(e){
        //window.location=$(this).attr("data-href");
        }
    });


/*    $(".mytemp").mCustomScrollbar({
          axis:"x",
          theme:"dark-thin",
          autoExpandScrollbar:true,
          advanced:{autoExpandHorizontalScroll:true}
        });*/

}
